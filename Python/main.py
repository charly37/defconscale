import serial

# your logic....should return an int between 1 and 5


def getSeverity():
    return 5


def updateScaleToLevel(iLevel):
    print("Updating to level: ", iLevel)
    #print("Open chanel")
    ser = serial.Serial('COM4', 9600, timeout=1)

    # print("Sending")
    ser.write(bytes([iLevel]))

    #print("Closing chanel")
    ser.close()


def main():
    print("Starting")

    aLevelToSet = getSeverity()
    updateScaleToLevel(aLevelToSet)

    print("Ending")


main()
