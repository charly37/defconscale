class DefconScale
{
  public:
    void update(int iLevel);
    void increase();
    void decrease();
    DefconScale(int iLevel1Pin, int iLevel2Pin, int iLevel3Pin, int iLevel4Pin, int iLevel5Pin);
    void setup();

    //Represent the thread level - from 0 to 6 
    //Levels 0 and 6 are special - 0 is no light ON and 6 is all ON
    int _threadLevel = 0;
    //pins of the light panels 
    int _level_1_pin;
    int _level_2_pin;
    int _level_3_pin;
    int _level_4_pin;
    int _level_5_pin;
};

DefconScale::DefconScale(int iLevel1Pin, int iLevel2Pin, int iLevel3Pin, int iLevel4Pin, int iLevel5Pin)
{
  this->_level_1_pin = iLevel1Pin;
  this->_level_2_pin = iLevel2Pin;
  this->_level_3_pin = iLevel3Pin;
  this->_level_4_pin = iLevel4Pin;
  this->_level_5_pin = iLevel5Pin;
}

void DefconScale::setup()
{
  pinMode(this->_level_1_pin, OUTPUT);
  pinMode(this->_level_2_pin, OUTPUT);
  pinMode(this->_level_3_pin, OUTPUT);
  pinMode(this->_level_4_pin, OUTPUT);
  pinMode(this->_level_5_pin, OUTPUT);

  //All light OFF
  this->update(0);
}

void DefconScale::increase()
{
  Serial.println("increase");
  if (_threadLevel<6){
    _threadLevel = _threadLevel + 1;
  }

  //update light panel
  this->update(_threadLevel);
}

void DefconScale::decrease()
{
  Serial.println("decrease");
  if (_threadLevel>0){
    _threadLevel = _threadLevel - 1;
  }

  //update light panel
  this->update(_threadLevel);
}

void DefconScale::update(int iLevel)
{
  //Dirty but i m lazy
  if (iLevel==0){
    digitalWrite(this->_level_1_pin, LOW);
    digitalWrite(this->_level_2_pin, LOW);
    digitalWrite(this->_level_3_pin, LOW);
    digitalWrite(this->_level_4_pin, LOW);
    digitalWrite(this->_level_5_pin, LOW);
  }
  else if (iLevel==1){
    digitalWrite(this->_level_1_pin, HIGH);
    digitalWrite(this->_level_2_pin, LOW);
    digitalWrite(this->_level_3_pin, LOW);
    digitalWrite(this->_level_4_pin, LOW);
    digitalWrite(this->_level_5_pin, LOW);
  }
  else if (iLevel==2){
    digitalWrite(this->_level_1_pin, LOW);
    digitalWrite(this->_level_2_pin, HIGH);
    digitalWrite(this->_level_3_pin, LOW);
    digitalWrite(this->_level_4_pin, LOW);
    digitalWrite(this->_level_5_pin, LOW);
  }
  else if (iLevel==3){
    digitalWrite(this->_level_1_pin, LOW);
    digitalWrite(this->_level_2_pin, LOW);
    digitalWrite(this->_level_3_pin, HIGH);
    digitalWrite(this->_level_4_pin, LOW);
    digitalWrite(this->_level_5_pin, LOW);
  }
  else if (iLevel==4){
    digitalWrite(this->_level_1_pin, LOW);
    digitalWrite(this->_level_2_pin, LOW);
    digitalWrite(this->_level_3_pin, LOW);
    digitalWrite(this->_level_4_pin, HIGH);
    digitalWrite(this->_level_5_pin, LOW);
  }
  else if (iLevel==5){
    digitalWrite(this->_level_1_pin, LOW);
    digitalWrite(this->_level_2_pin, LOW);
    digitalWrite(this->_level_3_pin, LOW);
    digitalWrite(this->_level_4_pin, LOW);
    digitalWrite(this->_level_5_pin, HIGH);
  }
  else if (iLevel==6){
    digitalWrite(this->_level_1_pin, HIGH);
    digitalWrite(this->_level_2_pin, HIGH);
    digitalWrite(this->_level_3_pin, HIGH);
    digitalWrite(this->_level_4_pin, HIGH);
    digitalWrite(this->_level_5_pin, HIGH);
  }
  //else{
  //  Serial.println("Error updating to level");
  //}
}

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////

//Create the light object with pin 6,9,10.
DefconScale _defconScale(8, 12, 9, 10, 11);
//Button
int _upButton = 6;
int _downButton = 7;
int _upButtonState = 0;
int _downButtonState = 0;
//For debug
int incomingByte;      // a variable to read incoming serial data into

void setup() {
  //USB Serial port.
  Serial.begin(9600);
  //Initialize the lights.
  _defconScale.setup();
  //Serial.println("Setup done");
  pinMode(_upButton,INPUT_PULLUP);
  pinMode(_downButton,INPUT_PULLUP);
}

void loop() {
  //update button state
  _upButtonState = digitalRead(_upButton);
  _downButtonState = digitalRead(_downButton);
  
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    incomingByte = Serial.read();
    int aValueInt = incomingByte;
    _defconScale.update(aValueInt);
    Serial.print("update: ");
    Serial.println(aValueInt);
  }
  if (_upButtonState == LOW) {
    _defconScale.increase();
  }
  if (_downButtonState == LOW) {
    _defconScale.decrease();
  }

  delay(150);
}
